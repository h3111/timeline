-- timeline.hs


import Data.Time
import System.Environment


timeLineSize :: Int
timeLineSize = 70

startDateActive, startDatePassive, startDateRetired  :: (Integer, Int, Int)
startDateActive  = (2019, 02, 01)
startDatePassive = (2021, 04, 01)
startDateRetired = (2023, 06, 01)


startDayActive, startDayPassive, startDayRetired:: Day
startDayActive  = fromGregorian' startDateActive
startDayPassive = fromGregorian' startDatePassive
startDayRetired = fromGregorian' startDateRetired

daysActive, daysPassive :: Int
daysActive  = fromInteger $ startDayPassive `diffDays` startDayActive
daysPassive = fromInteger $ startDayRetired `diffDays` startDayPassive

prefixActive, suffixActive, prefixPassive, suffixPassive :: String
prefixActive  = show daysActive <> "|"
suffixActive  = "|0"
prefixPassive = "0|"
suffixPassive = "|" <> show daysPassive

prefixBeforeActive, prefixRetired :: String
prefixBeforeActive = "Days left to start of partial retirement "
prefixRetired      = "RED "

prefixSuffixLengthActive, prefixSuffixLengthPassive :: Int
prefixSuffixLengthActive  = length $ mconcat [currentDate startDayActive,  prefixActive,  suffixActive]
prefixSuffixLengthPassive = length $ mconcat [currentDate startDayPassive, prefixPassive, suffixPassive]

prefixLengthBeforeActive, prefixLengthRetired :: Int
prefixLengthBeforeActive = length $ currentDate startDayActive  <> prefixBeforeActive
prefixLengthRetired      = length $ currentDate startDayPassive <> prefixRetired

fromGregorian' :: (Integer, Int, Int) -> Day
fromGregorian' (year, month, day) = fromGregorian year month day

scale :: Int -> Int -> Int -> Int
scale numerator denominator value
  | denominator == 0 = 0
  | otherwise        = value * numerator `div` denominator

mapper :: Int -> Int -> Int -> String
mapper dayDiff scaled x
  | scaled < x = "-"
  | scaled > x = "~"
  | otherwise = show dayDiff


currentDate :: Day -> String
currentDate = (<> ": ") . showGregorian

beforeActive :: Day -> String
beforeActive day
  | day < startDayActive = mconcat [prefixBeforeActive, replicate numDots '.', " ", show daysToStartActive]
  | otherwise = ""
  where
    daysToStartActive = startDayActive `diffDays` day
    numDots           = timeLineSize - sum [prefixLengthBeforeActive, length (show daysToStartActive), 1]


duringActive :: Day -> String
duringActive day
  | day >= startDayActive && day < startDayPassive = mconcat [prefixActive, line >>= mapper', suffixActive]
  | otherwise = ""
  where
    daysToEndActive = fromInteger $ startDayPassive `diffDays` day - 1
    start = timeLineSize - prefixSuffixLengthActive - length (show daysToEndActive)
    line = [start,start - 1 .. 0]
    mapper' :: Int -> String
    mapper' = mapper daysToEndActive $ scale start daysActive daysToEndActive

duringPassive :: Day -> String
duringPassive day
  | day >= startDayPassive && day < startDayRetired = mconcat [prefixPassive, line >>= mapper',  suffixPassive]
  | otherwise = ""
  where
    dayInPassive = fromInteger $ day `diffDays` startDayPassive + 1
    end = timeLineSize - prefixSuffixLengthPassive - length (show dayInPassive)
    line = [0 .. end]
    mapper' :: Int -> String
    mapper' = mapper dayInPassive $ scale end daysPassive dayInPassive

duringRetired :: Day -> String
duringRetired day
  | day >= startDayRetired = mconcat [prefixRetired, replicate numDots '.', " ", show dayInRetired]
  | otherwise = ""
  where
    dayInRetired = day `diffDays` startDayRetired + 1
    numDots      = timeLineSize - sum [prefixLengthRetired, length (show dayInRetired), 1]


timeLine :: Day -> String
timeLine = mconcat [currentDate, beforeActive, duringActive, duringPassive, duringRetired]

timeLineIO :: IO String
timeLineIO = timeLine . localDay . zonedTimeToLocalTime <$> getZonedTime


main :: IO ()
main = do
  args <- getArgs
  if null args || null (head args)
    then timeLineIO >>= putStrLn
    else mapM_ (putStrLn . timeLine) [addDays (-3) startDayActive .. addDays 2 startDayRetired]


-- EOF
